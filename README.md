# Eventtus Chat Application

Based on _AngularJS_

### Installation
Provided that **Nodejs** and **Ruby** are already installed

- npm install -g grunt-cli bower yo generator-karma generator-angular karma jshint
- gem install compass
- npm install
- bower install

## Build & Run

- Run `grunt build` for building
- Run `grunt serve` for serving
