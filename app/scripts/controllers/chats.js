"use strict";

/**
 * @ngdoc function
 * @name challengeApp.controller:ChatsCtrl
 * @description
 * # ChatsCtrl
 * Controller of the challengeApp
 */

(function () {

  function controller(scope, Chats, $q) {
    $q.when(Chats.$promise, function() {
      scope.chatFeed = Chats.items;
    });
  }
  angular.module("challengeApp")
  .controller("ChatsCtrl", ["$scope", "Chats", "$q", controller]);
})();
