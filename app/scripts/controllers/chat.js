"use strict";

/**
 * @ngdoc function
 * @name challengeApp.controller:ChatCtrl
 * @description
 * # ChatCtrl
 * Controller of the challengeApp
 */

(function () {

  function controller(scope, Chats, $q, $stateParams, Upload) {
    $q.when(Chats.$promise, function() {
      scope.activeConversation = _.find(Chats.items, function(o) {
        return o.id === Number($stateParams.id);
      });

      function newMessage(content, type) {
        var message = {
          isMine: true,
          type: type,
          content: content,
          sender: "https://avatars2.githubusercontent.com/u/7?v=3&s=56"
        };
        return message;
      }

      scope.send = function() {
        scope.activeConversation.messages.push(newMessage(this.text, "text"));
        this.text = "";
      };

      scope.upload = function(file) {
        Upload
          .base64DataUrl(file)
          .then(function(image) {
            if(image) {
              scope.activeConversation.messages.push(newMessage(image, "media"));
            }
          });
      };
    });
  }

  angular.module("challengeApp")
  .controller("ChatCtrl", ["$scope", "Chats", "$q", "$stateParams", "Upload", controller]);
})();
