"use strict";

/**
 * @ngdoc function
 * @name challengeApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the challengeApp
 */

(function () {

  function controller() {

  }
  angular.module("challengeApp")
  .controller("MainCtrl", [controller]);
})();
