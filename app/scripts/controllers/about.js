"use strict";

/**
 * @ngdoc function
 * @name challengeApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the challengeApp
 */

(function () {

  function controller() {

  }
  angular.module("challengeApp")
  .controller("AboutCtrl", [controller]);
})();
