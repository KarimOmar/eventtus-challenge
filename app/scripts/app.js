"use strict";

/**
* @ngdoc overview
* @name challengeApp
* @description
* # challengeApp
*
* Main module of the application.
*/

var modules = [
  "ngResource",
  "ngRoute",
  "ui.router",
  "yaru22.angular-timeago",
  "ngFileUpload",
  "luegg.directives"
];
var challengeApp = angular.module("challengeApp", modules);

challengeApp.config(["$compileProvider", "$stateProvider", "$urlRouterProvider", "$locationProvider",
  function ($compileProvider, $stateProvider, $urlRouterProvider, $locationProvider) {
    $compileProvider.debugInfoEnabled(false);
    $locationProvider.hashPrefix("");
    $urlRouterProvider.otherwise("/");

    $stateProvider
      .state("main", {
        url: "/",
        controller: "MainCtrl",
        templateUrl: "views/main.html",
        data: {
          pageTitle: "Home",
          pageDescription: "Eventtus Chat Application"
        }
      })
      .state("about", {
        url: "/about",
        controller: "AboutCtrl",
        templateUrl: "views/about.html",
        data: {
          pageTitle: "About",
          pageDescription: "Eventtus story"
        }
      })
      .state("chats", {
        url: "/chats",
        controller: "ChatsCtrl",
        templateUrl: "views/chats.html",
        data: {
          pageTitle: "Chats",
          pageDescription: "Try Eventtus first web chat application"
        }
      })
      .state("chats.chat", {
        url: "/:id",
        controller: "ChatCtrl",
        templateUrl: "views/chat.html",
        data: {
          pageTitle: "Chat",
          pageDescription: "Try Eventtus first web chat application"
        }
      });
  }
]);

challengeApp.run(["$rootScope",
  function(root) {
    root.setTitle = function(pageTitle) {
      var formatTitle = pageTitle + " :: Eventtus";
      root.pageTitle = formatTitle;
    };

    root.setDescription = function(pageDescription) {
      root.pageDescription = pageDescription;
    };

    root.$on("$stateChangeStart", function(event, toState){
      root.setTitle(toState.data.pageTitle);
      root.setDescription(toState.data.pageDescription);
    });

  }
]);
