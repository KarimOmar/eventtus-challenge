"use strict";

/**
 * @ngdoc function
 * @name challengeApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the challengeApp
 */

(function () {

  function Chats($http) {
    var items = [];
    var $promise;

    if (!items.length) {
      $promise = $http.get("../chat_feed.json").then(function(response) {
        response.data.forEach(function(item) {
          items.push(item);
        });
      });
    }

    return {
      items: items,
      $promise: $promise
    };
  }


  angular.module("challengeApp")
  .service("Chats", ["$http", Chats]);
})();
