"use strict";

/**
* @ngdoc directive
* @name challengeApp.directive:content
* @description
* # content
*/
(function () {

  function controller() {

  }
  angular.module("challengeApp")
  .directive("content", function () {
    return {
      templateUrl: "views/directives/content.html",
      restrict: "E",
      transclude: true,
      controller: [controller]
    };
  });
})();
