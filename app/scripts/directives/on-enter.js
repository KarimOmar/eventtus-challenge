"use strict";

/**
 * @ngdoc directive
 * @name challengeApp.directive:onEnter
 * @description
 * # onEnter
 */

angular.module("challengeApp").directive("onEnter", function () {
  return function (scope, element, attrs) {
    element.bind("keydown keypress", function (event) {
      if(event.which === 13) {
        scope.$apply(function (){
            scope.$eval(attrs.onEnter);
        });

        event.preventDefault();
      }
    });
  };
});
