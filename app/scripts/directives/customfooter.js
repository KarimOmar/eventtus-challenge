"use strict";

/**
* @ngdoc directive
* @name challengeApp.directive:customfooter
* @description
* # customfooter
*/
(function () {

  function controller() {

  }
  angular.module("challengeApp")
  .directive("customfooter", function () {
    return {
      templateUrl: "views/directives/customfooter.html",
      restrict: "E",
      replace: true,
      controller: [controller]
    };
  });
})();
