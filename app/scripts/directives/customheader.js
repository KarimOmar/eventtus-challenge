"use strict";

/**
* @ngdoc directive
* @name challengeApp.directive:customheader
* @description
* # customheader
*/
(function () {

  function controller() {

  }
  angular.module("challengeApp")
  .directive("customheader", function () {
    return {
      templateUrl: "views/directives/customheader.html",
      restrict: "E",
      replace: true,
      controller: [controller]
    };
  });
})();
